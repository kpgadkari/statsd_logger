# statsd_logger

This is a simple statsd echo server - it listens to port 8125 on localhost by default and prints received StatsD messages on stdout.

## Installation
```
pip install statsd_logger
```

## Running the server
```
statsd_logger
```

## License
This code is licensed under the GPLv3.
