FROM python:3.7-alpine

WORKDIR /opt
COPY ./requirements.txt ./requirements.txt
RUN pip install -r requirements.txt

EXPOSE 8125
CMD ["python", "-m", "statsd_logger"]
